package com.xdth.main;

import com.xdth.factory.FactoryUtil;
import com.xdth.mq.producer.QueueProducer;
import com.xdth.sync.command.RmtShellExecutor;
import com.xdth.sync.service.IDwInterfaceService;
import com.xdth.sync.util.SyncListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dy on 17-7-3.
 */
public class SyncMain {
    static final Logger LOG = LoggerFactory.getLogger(RmtShellExecutor.class);
    public static void main(String[] args) {
       // QueueProducer queueProducer = FactoryUtil.getBean("queueProducer", QueueProducer.class);
      //  queueProducer.sendText("SYNCQUEUE", null, "ALL");
        LOG.info("------开启数据同步监听------");
        SyncListener syncListener = FactoryUtil.getBean("syncListener", SyncListener.class);
        syncListener.startListener();
        System.out.println("------------");
    }
}
