package com.xdth.quartz.job;

import com.xdth.mq.producer.QueueProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by dy on 2017/7/7.
 */
@Component("syncJob")
public class SyncJob {

  Logger logger = LoggerFactory.getLogger(getClass());

  @Resource
  private QueueProducer queueProducer;
  @Value("#{config['sync.queue']}")
  private String queueName;

  public void execute(){
    logger.info("启动同步");
    queueProducer.sendText(queueName, null, "ALL");
  }
}
