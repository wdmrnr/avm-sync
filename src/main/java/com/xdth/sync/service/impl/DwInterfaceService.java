package com.xdth.sync.service.impl;

import com.xdth.sync.mapper.DwInterfaceMapper;
import com.xdth.sync.model.DmSyncLog;
import com.xdth.sync.model.DwInterface;
import com.xdth.sync.service.IDwInterfaceService;
import jodd.util.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dy on 17-7-3.
 */
@Service("dwInterfaceService")
public class DwInterfaceService implements IDwInterfaceService {

  @Resource
  private DwInterfaceMapper dwInterfaceMapper;

  @Override
  public List<DwInterface> findAllInterface() {
    return dwInterfaceMapper.findAllInterface();
  }

  @Override
  public void truncateTable(String tableName) {
    if (StringUtil.isNotEmpty(tableName)) {
      this.dwInterfaceMapper.truncateTable(tableName);
    }
  }

  @Override
  public DwInterface findDwInterface(String code) {
    if (StringUtil.isNotEmpty(code)) {
      return this.dwInterfaceMapper.findDwInterface(code);
    }
    return null;
  }

  @Override
  public Long interfaceDataCount(String tableName) {
    if (StringUtil.isNotEmpty(tableName)) {
      return this.dwInterfaceMapper.interfaceDataCount(tableName);
    }
    return null;
  }

  @Override
  public void insertLog(String fileName, Integer actualRow, Integer targetRow ,long startTime, boolean isSuccess, String syncLog ,String msg) {
    long syncTime = System.currentTimeMillis()/1000 - startTime;
    DmSyncLog dmSyncLog = new DmSyncLog(fileName, targetRow,  actualRow, Integer.valueOf(""+syncTime), isSuccess?"Y":"N", syncLog);
    dmSyncLog.setMsg(msg);
    dmSyncLog.setCode(fileName.substring(1, 6));
    this.dwInterfaceMapper.insertLog(dmSyncLog);
  }

  @Override
  public void execProc(String procName, String flag) {
    dwInterfaceMapper.execProc(procName, flag);
  }
}
