package com.xdth.sync.service;

import com.xdth.sync.model.DwInterface;

import java.util.Date;
import java.util.List;

/**
 * Created by dy on 17-7-3.
 */
public interface IDwInterfaceService {

  List<DwInterface> findAllInterface();

  void truncateTable(String tableName);

  DwInterface findDwInterface(String code);

  Long interfaceDataCount(String tableName);

  void insertLog(String fileName, Integer actualRow, Integer targetRow ,long startTime, boolean isSuccess, String syncLog, String msg);

  void execProc(String procName, String flag);

}
