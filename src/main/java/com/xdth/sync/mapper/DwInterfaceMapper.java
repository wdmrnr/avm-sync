package com.xdth.sync.mapper;

import com.xdth.sync.model.DmSyncLog;
import com.xdth.sync.model.DwInterface;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by dy on 17-7-3.
 */
public interface DwInterfaceMapper {
    List<DwInterface> findAllInterface();

    int truncateTable(@Param("tableName") String tableName);

    DwInterface findDwInterface(@Param("code") String code);

    Long interfaceDataCount(@Param("tableName") String tableName);

    void insertLog(DmSyncLog log);

    void execProc(@Param("procName") String procName, @Param("flag") String flag);
}
