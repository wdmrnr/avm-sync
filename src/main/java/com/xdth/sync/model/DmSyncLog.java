package com.xdth.sync.model;

import java.io.Serializable;

/**
 * Created by dy on 2017/7/6.
 */
public class DmSyncLog implements Serializable {
  private static final long serialVersionUID = -2562683559837845808L;

  private String file_name;
  private String code;
  private Integer target_row;
  private Integer actual_row;
  private Integer sync_time;
  private String sync_success;
  private String synclog;
  private String msg;

  public DmSyncLog(String file_name, Integer target_row, Integer actual_row, Integer sync_time, String sync_success, String synclog) {
    this.file_name = file_name;
    this.target_row = target_row;
    this.actual_row = actual_row;
    this.sync_time = sync_time;
    this.sync_success = sync_success;
    this.synclog = synclog;
  }

  public DmSyncLog() {
  }

  public String getFile_name() {
    return file_name;
  }

  public void setFile_name(String file_name) {
    this.file_name = file_name;
  }

  public Integer getTarget_row() {
    return target_row;
  }

  public void setTarget_row(Integer target_row) {
    this.target_row = target_row;
  }

  public Integer getActual_row() {
    return actual_row;
  }

  public void setActual_row(Integer actual_row) {
    this.actual_row = actual_row;
  }

  public Integer getSync_time() {
    return sync_time;
  }

  public void setSync_time(Integer sync_time) {
    this.sync_time = sync_time;
  }

  public String getSync_success() {
    return sync_success;
  }

  public void setSync_success(String sync_success) {
    this.sync_success = sync_success;
  }

  public String getSynclog() {
    return synclog;
  }

  public void setSynclog(String synclog) {
    this.synclog = synclog;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
