package com.xdth.sync.model;

import java.io.Serializable;

/**
 * Created by dy on 17-7-3.
 */
public class DwInterface implements Serializable {

    private String interfaceTable;
    private String InterfaceName;
    private String InterfaceFileCode;
    private String syncProc;
    private Integer status;

    public String getInterfaceTable() {
        return interfaceTable;
    }

    public void setInterfaceTable(String interfaceTable) {
        this.interfaceTable = interfaceTable;
    }

    public String getInterfaceName() {
        return InterfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        InterfaceName = interfaceName;
    }

    public String getInterfaceFileCode() {
        return InterfaceFileCode;
    }

    public void setInterfaceFileCode(String interfaceFileCode) {
        InterfaceFileCode = interfaceFileCode;
    }

    public String getSyncProc() {
        return syncProc;
    }

    public void setSyncProc(String syncProc) {
        this.syncProc = syncProc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
