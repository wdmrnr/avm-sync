package com.xdth.sync.model;


import java.io.Serializable;

public class MqMessage implements Serializable {

    private String key;
    private Object value;

    public MqMessage(){}
    public MqMessage(String key, Object value){
        this.key = key;
        this.value = value;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
