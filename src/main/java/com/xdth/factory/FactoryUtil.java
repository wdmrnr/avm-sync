package com.xdth.factory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Created by dy on 17-7-3.
 */
public class FactoryUtil {

    private final static ApplicationContext applicationContext = new FileSystemXmlApplicationContext("classpath:spring-db.xml", "classpath:spring-mq.xml", "classpath:spring-task.xml");

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(String name, Class<T> clasz) {
        return applicationContext.getBean(name, clasz);
    }

    public static <T> T getBean(Class<T> clasz) {
        return applicationContext.getBean(clasz);
    }

}
