package com.xdth.mq.consumer;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("queueConsumer")
public class QueueConsumer extends MqConsumer {

    @Resource(name="jmsTemplate")
    private JmsTemplate jmsTemplate;


    @Override
    public JmsTemplate getTemplate() {
        return jmsTemplate;
    }
}
