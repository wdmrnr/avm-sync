package com.xdth.mq.producer;

import com.xdth.sync.model.MqMessage;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.support.JmsGatewaySupport;

import javax.jms.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by dy on 17-5-18.
 */
public class MqProducer extends JmsGatewaySupport {

  public void sendMap(String destinationName, final List<MqMessage> list) {
    this.getJmsTemplate().send(destinationName, new MessageCreator() {
      public Message createMessage(Session session) throws JMSException {
        MapMessage message = session.createMapMessage();
        if (null != list) {
          for (MqMessage msg : list) {
            message.setObject(msg.getKey(), msg.getValue());
          }
        }

        return message;
      }
    });
  }

  public void sendText(String destinationName, final List<MqMessage> list, final String content) {
    this.getJmsTemplate().send(destinationName, new MessageCreator() {
      public Message createMessage(Session session) throws JMSException {
        TextMessage message = session.createTextMessage();
        if (null != list) {
          for (MqMessage msg : list) {
            message.setObjectProperty(msg.getKey(), msg.getValue());
          }
        }
        message.setText(content);
        return message;
      }
    });
  }

  public void sendObject(String destinationName,  final Serializable content) {
    this.getJmsTemplate().send(destinationName, new MessageCreator() {
      public Message createMessage(Session session) throws JMSException {
        ObjectMessage message = session.createObjectMessage();

        message.setObject(content);
        return message;
      }
    });
  }
}
